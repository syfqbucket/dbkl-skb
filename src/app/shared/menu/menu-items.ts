export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    isCollapsed?: boolean;
    isCollapsing?: any;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    type?: string;
    collapse?: string;
    children?: ChildrenItems2[];
    isCollapsed?: boolean;
}
export interface ChildrenItems2 {
    path?: string;
    title?: string;
    type?: string;
}
//Menu Items
export const ROUTES: RouteInfo[] = [
  {
    path: '/admin/dashboard',
    title: 'Dashboard',
    type: 'link',
    icontype: 'fas fa-desktop text-orange'
  },
  {
    path: '/admin/application',
    title: 'Permohonan',
    type: 'link',
    icontype: 'fab fa-wpforms text-indigo'
  },
  {
    path: '/admin/enforcement',
    title: 'Penguatkuasaan',
    type: 'link',
    icontype: 'fas fa-user-secret text-yellow'
  },
  {
    path: '/admin/inspection',
    title: 'Pemeriksaan',
    type: 'link',
    icontype: 'fas fa-search-location text-blue'
  },
  {
    path: '/admin/monitoring',
    title: 'Pemantauan Pembinaan',
    type: 'link',
    icontype: 'fas fa-binoculars text-green'
  },
  {
    path: '/admin/report',
    title: 'Laporan',
    type: 'link',
    icontype: 'fas fa-desktop text-red'
  },
  {
    path: '/admin/administrator',
    title: 'Pentadbir Sistem',
    type: 'link',
    icontype: 'fas fa-user-shield text-gray'
  }
];
