import { Component, OnInit } from '@angular/core';

const data = {
  colorrange: {
    gradient: "0",
    color: [
      {
        code: "#6da81e",
        minvalue: "0",
        maxvalue: "50",
        label: "Normal"
      },
      {
        code: "#f6bc33",
        minvalue: "50",
        maxvalue: "70",
        label: "Pemerhatian"
      },
      {
        code: "#e24b1a",
        minvalue: "70",
        maxvalue: "85",
        label: "Kritikal"
      }
    ]
  },
  dataset: [
    {
      data: [
        {
          rowid: "JH",
          columnid: "JA",
          displayvalue: "60.1",
          colorrangelabel: "Pemerhatian"
        },
        {
          rowid: "JH",
          columnid: "AP",
          displayvalue: "64.5",
          colorrangelabel: "Pemerhatian"
        },
        {
          rowid: "JH",
          columnid: "FE",
          displayvalue: "68.2",
          colorrangelabel: "Pemerhatian"
        },
        {
          rowid: "JH",
          columnid: "MA",
          displayvalue: "65.7",
          colorrangelabel: "Pemerhatian"
        },
        {
          rowid: "ML",
          columnid: "JA",
          displayvalue: "33.7",
          colorrangelabel: "Normal"
        },
        {
          rowid: "ML",
          columnid: "AP",
          displayvalue: "57.8",
          colorrangelabel: "Pemerhatian"
        },
        {
          rowid: "ML",
          columnid: "FE",
          displayvalue: "74.49",
          colorrangelabel: "Kritikal"
        },
        {
          rowid: "ML",
          columnid: "MA",
          displayvalue: "57.6",
          colorrangelabel: "Pemerhatian"
        },
        {
          rowid: "KN",
          columnid: "JA",
          displayvalue: "22.89",
          colorrangelabel: "Normal"
        },
        {
          rowid: "KN",
          columnid: "AP",
          displayvalue: "55.7",
          colorrangelabel: "Kritikal"
        },
        {
          rowid: "KN",
          columnid: "FE",
          displayvalue: "72.2",
          colorrangelabel: "Kritikal"
        },
        {
          rowid: "KN",
          columnid: "MA",
          displayvalue: "51.6",
          colorrangelabel: "Pemerhatian"
        },
        {
          rowid: "PP",
          columnid: "JA",
          displayvalue: "53.0",
          colorrangelabel: "Pemerhatian"
        },
        {
          rowid: "PP",
          columnid: "AP",
          displayvalue: "72.7",
          colorrangelabel: "Kritikal"
        },
        {
          rowid: "PP",
          columnid: "FE",
          displayvalue: "83.3",
          colorrangelabel: "Kritikal"
        },
        {
          rowid: "PP",
          columnid: "MA",
          displayvalue: "53.0",
          colorrangelabel: "Pemerhatian"
        }
      ]
    }
  ],
  columns: {
    column: [
      {
        id: "JA",
        label: "Januari"
      },
      {
        id: "FE",
        label: "Februari"
      },
      {
        id: "MA",
        label: "Mac"
      },
      {
        id: "AP",
        label: "April"
      }
    ]
  },
  rows: {
    row: [
      {
        id: "ML",
        label: "Kawasan 1"
      },
      {
        id: "JH",
        label: "Kawasan 2"
      },
      {
        id: "KN",
        label: "Kawasan 3"
      },
      {
        id: "PP",
        label: "Kawasan 4"
      }
    ]
  },
  chart: {
    theme: "fusion",
    showvalues: "1",
    mapbycategory: "1",
    plottooltext:
      "Purata operasi di $rowlabel pada bulan $columnlabel ialah $displayvalue"
  }
};

const data2 = {
  chart: {
    theme: "fusion",
    drawcrossline: "1",
    plotfillalpha: "50",
    plottooltext:
      "<b>$dataValue</b>: <b>$label</b>"
  },
  categories: [
    {
      category: [
        {
          label: "Mon"
        },
        {
          label: "Tue"
        },
        {
          label: "Wed"
        },
        {
          label: "Thu"
        },
        {
          label: "Fri"
        },
        {
          label: "Sat"
        },
        {
          label: "Sun"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "2018",
      data: [
        {
          value: "15"
        },
        {
          value: "15"
        },
        {
          value: "15"
        },
        {
          value: "20"
        },
        {
          value: "29"
        },
        {
          value: "18"
        },
        {
          value: "11"
        }
      ]
    },
    {
      seriesname: "2019",
      data: [
        {
          value: "20"
        },
        {
          value: "26"
        },
        {
          value: "19"
        },
        {
          value: "15"
        },
        {
          value: "17"
        },
        {
          value: "11"
        },
        {
          value: "18"
        }
      ]
    }
  ]
};

const data3 = {
  chart: {
    theme: "fusion",
    showvalues: "0",
    showplotborder: "1",
    plotbordercolor: "#FFFFFF",
    plotborderthickness: "3",
    showshadow: "0",
    placexaxislabelsontop: "1",
    labelfontsize: "16",
    showtooltip: "0",
    usehovereffect: "0"
  },
  dataset: [
    {
      data: [
        {
          rowid: "Kedah",
          columnid: "Januari",
          value: "6"
        },
        {
          rowid: "Kedah",
          columnid: "Februari",
          value: "7"
        },
        {
          rowid: "Kedah",
          columnid: "Mac",
          value: "8"
        },
        {
          rowid: "Kedah",
          columnid: "April",
          value: "5"
        },
        {
          rowid: "Pulau Pinang",
          columnid: "Januari",
          value: "5"
        },
        {
          rowid: "Pulau Pinang",
          columnid: "Februari",
          value: "7"
        },
        {
          rowid: "Pulau Pinang",
          columnid: "Mac",
          value: "4"
        },
        {
          rowid: "Pulau Pinang",
          columnid: "April",
          value: "3"
        },
        {
          rowid: "Melaka",
          columnid: "Januari",
          value: "3"
        },
        {
          rowid: "Melaka",
          columnid: "Februari",
          value: "4"
        },
        {
          rowid: "Melaka",
          columnid: "Mac",
          value: "5"
        },
        {
          rowid: "Melaka",
          columnid: "April",
          value: "4"
        },
        {
          rowid: "Johor",
          columnid: "Januari",
          value: "6"
        },
        {
          rowid: "Johor",
          columnid: "Februari",
          value: "3"
        },
        {
          rowid: "Johor",
          columnid: "Mac",
          value: "4"
        },
        {
          rowid: "Johor",
          columnid: "April",
          value: "2"
        },
        {
          rowid: "Perak",
          columnid: "Januari",
          value: "3"
        },
        {
          rowid: "Perak",
          columnid: "Februari",
          value: "2"
        },
        {
          rowid: "Perak",
          columnid: "Mac",
          value: "3"
        },
        {
          rowid: "Perak",
          columnid: "April",
          value: "2"
        },
        {
          rowid: "Pahang",
          columnid: "Januari",
          value: "3"
        },
        {
          rowid: "Pahang",
          columnid: "Februari",
          value: "2"
        },
        {
          rowid: "Pahang",
          columnid: "Mac",
          value: "3"
        },
        {
          rowid: "Pahang",
          columnid: "April",
          value: "2"
        },
        {
          rowid: "Terengganu",
          columnid: "Januari",
          value: "2"
        },
        {
          rowid: "Terengganu",
          columnid: "Februari",
          value: "1"
        },
        {
          rowid: "Terengganu",
          columnid: "Mac",
          value: "2"
        },
        {
          rowid: "Terengganu",
          columnid: "April",
          value: "1"
        },
        {
          rowid: "Sarawak",
          columnid: "Januari",
          value: "1"
        },
        {
          rowid: "Sarawak",
          columnid: "Februari",
          value: "1"
        },
        {
          rowid: "Sarawak",
          columnid: "Mac",
          value: "3"
        },
        {
          rowid: "Sarawak",
          columnid: "April",
          value: "2"
        },
        {
          rowid: "Sabah",
          columnid: "Januari",
          value: "2"
        },
        {
          rowid: "Sabah",
          columnid: "Februari",
          value: "4"
        },
        {
          rowid: "Sabah",
          columnid: "Mac",
          value: "3"
        },
        {
          rowid: "Sabah",
          columnid: "April",
          value: "1"
        },
        {
          rowid: "Perlis",
          columnid: "Januari",
          value: "4"
        },
        {
          rowid: "Perlis",
          columnid: "Februari",
          value: "2"
        },
        {
          rowid: "Perlis",
          columnid: "Mac",
          value: "5"
        },
        {
          rowid: "Perlis",
          columnid: "April",
          value: "4"
        },
        {
          rowid: "Kelantan",
          columnid: "Januari",
          value: "4"
        },
        {
          rowid: "Kelantan",
          columnid: "Februari",
          value: "2"
        },
        {
          rowid: "Kelantan",
          columnid: "Mac",
          value: "5"
        },
        {
          rowid: "Kelantan",
          columnid: "April",
          value: "4"
        },
        {
          rowid: "Selangor",
          columnid: "Januari",
          value: "4"
        },
        {
          rowid: "Selangor",
          columnid: "Februari",
          value: "4"
        },
        {
          rowid: "Selangor",
          columnid: "Mac",
          value: "6"
        },
        {
          rowid: "Selangor",
          columnid: "April",
          value: "4"
        },
        {
          rowid: "Negeri Sembilan",
          columnid: "Januari",
          value: "5"
        },
        {
          rowid: "Negeri Sembilan",
          columnid: "Februari",
          value: "4"
        },
        {
          rowid: "Negeri Sembilan",
          columnid: "Mac",
          value: "2"
        },
        {
          rowid: "Negeri Sembilan",
          columnid: "April",
          value: "4"
        },
        {
          rowid: "Kelantan",
          columnid: "Januari",
          value: "7"
        },
        {
          rowid: "Kelantan",
          columnid: "Februari",
          value: "5"
        },
        {
          rowid: "Kelantan",
          columnid: "Mac",
          value: "6"
        },
        {
          rowid: "Kelantan",
          columnid: "April",
          value: "8"
        }
      ]
    }
  ],
  colorrange: {
    gradient: "1",
    minvalue: "-8",
    code: "#862d2d",
    startlabel: "Cepat",
    endlabel: "Lambat",
    color: [
      {
        code: "#ff9933",
        maxvalue: "4"
      },
      {
        code: "#FFFFFF",
        maxvalue: "0"
      },
      {
        code: "#9ae5e5",
        maxvalue: "4"
      },
      {
        code: "#004d4d",
        maxvalue: "8"
      }
    ]
  }
};


@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss']
})
export class MonitoringComponent implements OnInit {

  width = '100%'
  height = '100%'
  dataFormat = "json"

  type = "heatmap"
  dataSource = data

  type2 = "inversemsarea"
  dataSource2 = data2

  type3 = "heatmap"
  dataSource3 = data3

  map

  constructor() { }

  ngOnInit() {
  }

}
