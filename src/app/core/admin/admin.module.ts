import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  BsDropdownModule, 
  ProgressbarModule, 
  TooltipModule, 
  BsDatepickerModule,
  ModalModule
} from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';

import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as Widgets from 'fusioncharts/fusioncharts.widgets';
import * as PowerCharts from 'fusioncharts/fusioncharts.powercharts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
FusionChartsModule.fcRoot(
  FusionCharts, 
  Charts,
  Widgets,
  FusionTheme,
  PowerCharts
);

import { AdminRoutes } from './admin.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ReportComponent } from './report/report.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { ApplicationComponent } from './application/application.component';
import { EnforcementComponent } from './enforcement/enforcement.component';
import { InspectionComponent } from './inspection/inspection.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

@NgModule({
  declarations: [
    DashboardComponent,
    ReportComponent,
    AdministratorComponent,
    MonitoringComponent,
    ApplicationComponent,
    EnforcementComponent,
    InspectionComponent
  ],
  imports: [
    CommonModule,
    BsDropdownModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    RouterModule.forChild(AdminRoutes),
    FusionChartsModule,
    LeafletModule,
    BsDatepickerModule.forRoot(),
    NgxMapboxGLModule,
    ModalModule.forRoot()
  ]
})
export class AdminModule { }
