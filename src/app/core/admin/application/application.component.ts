import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";


const data = {
  chart: {
    enablesmartlabels: "1",
    showlabels: "1",
    usedataplotcolorforlabels: "1",
    plottooltext: "$label, <b>$value</b>",
    theme: "fusion"
  },
  data: [
    {
      label: "Kawasan 1",
      value: "290"
    },
    {
      label: "Kawasan 2",
      value: "260"
    },
    {
      label: "Kawasan 3",
      value: "180"
    },
    {
      label: "Kawasan 4",
      value: "140"
    },
    {
      label: "Kawasan 5",
      value: "115"
    }
  ]
};


const data2 = {
  chart: {
    formatnumberscale: "0",
    showvalues: "0",
    drawcrossline: "1",
    showsum: "1",
    plottooltext: "$dataValue $seriesName",
    theme: "fusion"
  },
  categories: [
    {
      category: [
        {
          label: "Jan"
        },
        {
          label: "Feb"
        },
        {
          label: "Mar"
        },
        {
          label: "Apr"
        },
        {
          label: "May"
        },
        {
          label: "Jun"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "Kawasan 1",
      data: [
        {
          value: "400"
        },
        {
          value: "830"
        },
        {
          value: "500"
        },
        {
          value: "420"
        },
        {
          value: "790"
        },
        {
          value: "380"
        }
      ]
    },
    {
      seriesname: "Kawasan 2",
      data: [
        {
          value: "350"
        },
        {
          value: "620"
        },
        {
          value: "410"
        },
        {
          value: "370"
        },
        {
          value: "720"
        },
        {
          value: "310"
        }
      ]
    },
    {
      seriesname: "Kawasan 3",
      data: [
        {
          value: "210"
        },
        {
          value: "400"
        },
        {
          value: "450"
        },
        {
          value: "180"
        },
        {
          value: "570"
        },
        {
          value: "270"
        }
      ]
    },
    {
      seriesname: "Kawasan 4",
      data: [
        {
          value: "180"
        },
        {
          value: "330"
        },
        {
          value: "230"
        },
        {
          value: "160"
        },
        {
          value: "440"
        },
        {
          value: "350"
        }
      ]
    },
    {
      seriesname: "Kawasan 5",
      data: [
        {
          value: "60"
        },
        {
          value: "200"
        },
        {
          value: "200"
        },
        {
          value: "50"
        },
        {
          value: "230"
        },
        {
          value: "150"
        }
      ]
    }
  ]
};

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {

  width = '100%'
  height = '100%'
  dataFormat = 'json'

  type = 'doughnut3d'
  dataSource = data

  type2 = 'stackedarea2d'
  dataSource2 = data2

  defaultModal: BsModalRef;
  default = {
    keyboard: true,
    class: "modal-dialog-centered"
  };

  constructor(
    private modalService: BsModalService
  ) { }

  ngOnInit() {
  }

  doOpenModal(modalDefault: TemplateRef<any>) {
    this.defaultModal = this.modalService.show(modalDefault, this.default);
  }

}
