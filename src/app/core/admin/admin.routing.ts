import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { ApplicationComponent } from './application/application.component';
import { EnforcementComponent } from './enforcement/enforcement.component';
import { InspectionComponent } from './inspection/inspection.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { ReportComponent } from './report/report.component';

export const AdminRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'administrator',
                component: AdministratorComponent
            },
            {
                path: 'application',
                component: ApplicationComponent
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'enforcement',
                component: EnforcementComponent
            },
            {
                path: 'inspection',
                component: InspectionComponent
            },
            {
                path: 'monitoring',
                component: MonitoringComponent
            },
            {
                path: 'report',
                component: ReportComponent
            },
            {
                path: 'administrator',
                component: AdministratorComponent
            }
        ]
    }
]