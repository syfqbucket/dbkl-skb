import { Component, OnInit } from '@angular/core';

const data = {
  chart: {
    showhovereffect: "1",
    drawcrossline: "1",
    plottooltext: "<b>$dataValue</b> $seriesName",
    theme: "fusion"
  },
  categories: [
    {
      category: [
        {
          label: "2012"
        },
        {
          label: "2013"
        },
        {
          label: "2014"
        },
        {
          label: "2015"
        },
        {
          label: "2016"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "Registered",
      data: [
        {
          value: "620"
        },
        {
          value: "640"
        },
        {
          value: "604"
        },
        {
          value: "606"
        },
        {
          value: "780"
        }
      ]
    },
    {
      seriesname: "Active",
      data: [
        {
          value: "160"
        },
        {
          value: "280"
        },
        {
          value: "340"
        },
        {
          value: "420"
        },
        {
          value: "540"
        }
      ]
    },
    {
      seriesname: "Inactive",
      data: [
        {
          value: "120"
        },
        {
          value: "122"
        },
        {
          value: "127"
        },
        {
          value: "122"
        },
        {
          value: "129"
        }
      ]
    },
    {
      seriesname: "Banned",
      data: [
        {
          value: "18"
        },
        {
          value: "19"
        },
        {
          value: "21"
        },
        {
          value: "21"
        },
        {
          value: "24"
        }
      ]
    }
  ]
};

const data2 = {
  chart: {
    showpercentvalues: "1",
    defaultcenterlabel: "Users",
    aligncaptionwithcanvas: "0",
    captionpadding: "0",
    decimals: "1",
    plottooltext:
      "<b>$percentValue</b> <b>$label</b>",
    centerlabel: "# Users: $value",
    theme: "fusion"
  },
  data: [
    {
      label: "Active",
      value: "5000"
    },
    {
      label: "Inactive",
      value: "2000"
    },
    {
      label: "Banned",
      value: "500"
    }
  ]
};

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.scss']
})
export class AdministratorComponent implements OnInit {

  width = '100%'
  height = '100%'
  dataFormat = 'json'

  type = 'msline'
  dataSource = data

  type2 = 'doughnut2d'
  dataSource2 = data2

  constructor() { }

  ngOnInit() {
  }

}
