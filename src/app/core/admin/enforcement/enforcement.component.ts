import { Component, OnInit } from '@angular/core';
import * as hotspots from './hotspot.json';
import { Layer } from 'mapbox-gl';

@Component({
  selector: 'app-enforcement',
  templateUrl: './enforcement.component.html',
  styleUrls: ['./enforcement.component.scss']
})
export class EnforcementComponent implements OnInit {

  earthquakes: object;

  clusterLayers: Layer[];

  async ngOnInit() {
    this.earthquakes = await import('./hotspot.json');
    const layersData: [number, string][] = [
      [0, 'green'],
      [3, 'orange'],
      [5, 'red']
    ];
    this.clusterLayers = layersData.map((data, index) => ({
      id: `cluster-${index}`,
      paint: {
        'circle-color': data[1],
        'circle-radius': 70,
        'circle-blur': 1
      },
      filter: index === layersData.length - 1 ?
        ['>=', 'point_count', data[0]] :
        ['all',
          ['>=', 'point_count', data[0]],
          ['<', 'point_count', layersData[index + 1][0]]
        ]
    }));
  }
  labelLayerId

  constructor() { }
  mapLoaded(e){

  }

}
