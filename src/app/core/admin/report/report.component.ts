import { Component, OnInit } from '@angular/core';

const data = {
  chart: {
    theme: "fusion"
  },
  categories: [
    {
      category: [
        {
          label: "January"
        },
        {
          label: "February"
        },
        {
          label: "March"
        },
        {
          label: "April"
        },
        {
          label: "May"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "2018",
      data: [
        {
          value: "121"
        },
        {
          value: "67"
        },
        {
          value: "70"
        },
        {
          value: "55"
        },
        {
          value: "42"
        }
      ]
    },
    {
      seriesname: "2019",
      data: [
        {
          value: "115"
        },
        {
          value: "147"
        },
        {
          value: "239"
        },
        {
          value: ""
        },
        {
          value: "57"
        }
      ]
    }
  ]
};

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();

  width = '100%';
  height = '100%';
  type = "msbar3d";
  dataFormat = "json";
  dataSource = data;

  constructor() { 
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate];
  }

  ngOnInit() {
  }

}
